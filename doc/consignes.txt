Pour tester un client ftp, nous vous proposons un serveur ftp (en Python). Pour le lancer :
Récupérez et décompressez l'archive ci-dessous. Elle contient :
Le serveur python dans le sous-répertoire pyftpdlib
Le script ftpserv.py pour lancer le serveur
Un répertoire ftp servant de racine pour votre serveur et contenant quelques fichiers
À partir d'un terminal, placez-vous dans le répertoire nouvellement créé (ftpserver).
Exécutez le script python avec comme argument le numéro de port que vous voulez pour votre canal de contrôle FTP (l'équivalent du port 21). Si pas d'argument, ce sera par défaut le port 9876.
login:~ $ cd ftpserver
login:~/ftpserver $ ./ftpserv.py 2121
Pour tester ce serveur, vous pouvez utiliser par exemple :
votre navigateur avec l'url ftp://anonymous@localhost:9876,
la commande ftp (puis utiliser la commande "help" pour voir la liste des commandes du client ftp)
un client ftp graphique (genre fillezilla, gftp, ... à installer)
la commande telnet localhost 9876,
la commande nc localhost 9876 (idem telnet)
Les deux dernières solutions vous permettrons de gérer vous-même précisément ce qui est envoyé par votre client (les requêtes) et de voir réellement ce qui se passe en retour. 
