package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
	Socket mySocket;
	InputStreamReader inputStreamReader;
	BufferedReader inputBufferedReader;
	BufferedReader in;
	PrintWriter out;
	String IP;
	int port;
	
	public static void main (String[] args) throws IOException {
		Client client = new Client();
		
		client.launch();
		
		
	}
	
	private Client() throws IOException {
		System.out.println("Nom de l'hote :");
		inputStreamReader = new InputStreamReader(System.in);
		inputBufferedReader = new BufferedReader(inputStreamReader);
		String host = inputBufferedReader.readLine();
		port = 2121;
		mySocket = new Socket(host, port);
		
		String inetAddress = mySocket.getInetAddress().toString();
		String[] inetAddressSplited = inetAddress.split("/");
		IP = inetAddressSplited[1].replace('.', ',');
		System.out.println("Connecte :" + IP);
		
		in = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
		out = new PrintWriter(mySocket.getOutputStream(), true);
	}
	
	private void launch () throws IOException {
		
		
		
		boolean userOK = false;
		boolean passwordOK = false;
		while (!passwordOK) {
			while (!userOK) {
				System.out.println("Utilisateur :");
				inputStreamReader = new InputStreamReader(System.in);
				out.println("user " + inputBufferedReader.readLine());
				in.readLine();
				if (in.readLine().substring(0, 3).equals("331")) {
					userOK = true;
					System.out.println("Utilisateur confirme\n");
				}
			}
		
			System.out.println("Mot de passe :");
			inputStreamReader = new InputStreamReader(System.in);
			out.println("pass " + inputBufferedReader.readLine());
			if (in.readLine().substring(0, 3).equals("230")) { 
				passwordOK = true;
				System.out.println("Mot de passe accepte\n");
			}
			else { 
				userOK = false;
				System.out.println("Mot de passe invalide, retour à l'utilisateur\n");
			}
		}		
		
		boolean quit = false;
		while (!quit) {
			System.out.println("Choix du mode : actif / passif	(! pour quitter)");
			inputStreamReader = new InputStreamReader(System.in);
			String choice = inputBufferedReader.readLine().toLowerCase();
			if (choice.equals("actif")) {
				System.out.println("Lancement du mode actif\n");
				actif();
			}
			else if (choice.equals("passif")) {
				System.out.println("Lancement du mode passif\n");
				passif();
			}
			else if (choice.equals("!")) {
				System.out.println("Fermeture");
				quit = true;
			}
		}
	}
	
	public void actif () throws IOException {
		String choice;
		String answer;
		boolean getBack = false;
		while (!getBack) {
			System.out.println("Commande (! pour revenir en arriere) :");
			inputStreamReader = new InputStreamReader(System.in);
			choice = inputBufferedReader.readLine().toLowerCase();
			
			if (choice.equals("!")) {
				getBack = true;
			}
			else {
				out.println("PORT " + IP + "," + (port/256) + "," + (port%256));
				out.println(choice);
				
				answer = in.toString();
				
				System.out.println(answer);
				// Envoi commande
				
				// Réception et affichage réponse
			}
		}
		System.out.println("Retour aux choix du mode\n");		
	}
	
	public void passif () {
		// Prise commande, demande aide ou retour arrière
		
		// Envoi commande
		
		// Réception et affichage réponse
	}
}