package commands;

import util.CommandExecutor;

public enum Command {
	CWD("CWD", "Syntax: CWD [<SP> dir-name] (change working directory)."),
	HELP("HELP","Syntax: HELP [<SP> cmd] (show help)."),
	LIST("LIST", "Syntax: LIST [<SP> path] (list files)."),
	MODE("MODE", "Syntax: MODE <SP> mode (noop; set data transfer mode)."),
	NOOP("NOOP", "Syntax: NOOP (just do nothing)."),
	PASS("PASS", "Syntax: PASS [<SP> password] (set user password)."),
	PASV("PASV", "Syntax: PASV (open passive data connection)."),
	PORT("PORT", "Syntax: PORT <sp> h,h,h,h,p,p (open active data connection)."),
	PWD("PWD", "PWD (get current working directory)."),
	QUIT("QUIT", "Syntax: QUIT (quit current session)."),
	RETR("RETR", "Syntax: RETR <SP> file-name (retrieve a file)."),
	STOR("STOR", "Syntax: STOR <SP> file-name (store a file)."),
	TYPE("TYPE", "Syntax: TYPE <SP> [A | I] (set transfer type)."),
	USER("USER", "Syntax: USER <SP> user-name (set username).");
	
	private String command;
	private String help;
	private CommandExecutor exe; //permettra un refactoring
	
	private Command(String command) {
		this(command, "No help found");
	}
	
	private Command(String command, String help) {
		this.command = command;
		this.help = help;
	}
	
	public String getCommand() {
		return command;
	}

	public String getHelp() {
		return help;
	}
}
