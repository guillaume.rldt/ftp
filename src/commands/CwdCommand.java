package commands;

import java.io.File;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;
import util.UserStat;
import main.ServerMain;

public class CwdCommand implements CommandExecutor{

	public CommandStat executeCommand(Connection connection, String command, String[] args) {

		if(!(connection.getUser().getUserStat() == UserStat.CONNECTED || connection.getUser().getUserStat() == UserStat.CONNECTED_WITHOUT_DATAPORT)) {
			connection.sendMessage("530 Log in with USER and PASS first.");
			return CommandStat.FAILED;
		}
		if (args.length <= 1) {
			connection.sendMessage("501 Syntax error: command needs an argument");
			return CommandStat.FAILED;
		}
		File file = connection.getUser().getCurrentPath();
		if(args[1].equals("..")) {
			if(!ServerMain.getDefaultFolder().equals(connection.getUser().getCurrentPath())) { // pour eviter de revenir trop en arriere
				file = file.getParentFile();
			}
		}
		else {
			file = new File(connection.getUser().getCurrentPath(), args[1]);
			System.out.println(file.getAbsolutePath());
			if(!file.exists()) {
				connection.sendMessage("550 No such file or directory.");
				return CommandStat.FAILED;
			}
			if(file.isFile()) {
				connection.sendMessage("550 Not a directory.");
				return CommandStat.FAILED;
			}
		}
		connection.getUser().setCurrentPath(file);
		ServerMain.executeCommand(connection, "pwd", null);
		return CommandStat.SUCCESSED;
	}
}

