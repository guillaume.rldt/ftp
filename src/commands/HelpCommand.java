package commands;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;

public class HelpCommand implements CommandExecutor{

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		String message = "";
		if (args.length <= 1) {
			message = allCommand();
		}
		else {
			message = "214 " + showHelpOnCommand(args[1]);
		}
		connection.sendMessage(message);
		return CommandStat.SUCCESSED;
	}
	
	private String allCommand() {
		String message = "214-The following commands are recognized:\n";
		for(Command cmd : Command.values())
			message += " " + cmd.getCommand();
		return message;
	}
	
	private String showHelpOnCommand(String arg) {
		switch (arg.toUpperCase()) {
		case "USER":
			return Command.USER.getHelp();
		case "PASS":
			return Command.PASS.getHelp();
		case "HELP":
			return Command.HELP.getHelp();
		case "CWD":
			return Command.CWD.getHelp();
		case "PASV":
			return Command.PASV.getHelp();
		case "PORT":
			return Command.PORT.getHelp();
		case "QUIT":
			return Command.QUIT.getHelp();
		case "LIST":
			return Command.LIST.getHelp();
		case "MODE":
			return Command.MODE.getHelp();
		case "NOOP":
			return Command.NOOP.getHelp();
		case "RETR":
			return Command.RETR.getHelp();
		case "STOR":
			return Command.STOR.getHelp();
		case "TYPE":
			return Command.TYPE.getHelp();
		default:
			return "501 Unrecognized command";
		}
	}
}
