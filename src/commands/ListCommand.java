package commands;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;
import util.UserStat;

public class ListCommand implements CommandExecutor {

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		
		if(!(connection.getUser().getUserStat() == UserStat.CONNECTED || connection.getUser().getUserStat() == UserStat.CONNECTED_WITHOUT_DATAPORT)) {
			connection.sendMessage("530 Log in with USER and PASS first.");
			return CommandStat.FAILED;
		}
		
		connection.sendMessage("500 WIP");
		return CommandStat.UNKNOW_COMMAND;
	}
}
