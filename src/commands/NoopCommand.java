package commands;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;

public class NoopCommand  implements CommandExecutor {

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		connection.sendMessage("214 Syntax: NOOP (just do nothing).");
		return CommandStat.SUCCESSED;
	}
}
