package commands;

import java.time.LocalDateTime;

import main.Connection;
import main.ServerMain;
import util.CommandExecutor;
import util.CommandStat;
import util.Log;
import util.PrinterLog;
import util.UserStat;

public class PassCommand implements CommandExecutor{

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		
		if (args.length <= 1) {
			connection.sendMessage("501 Syntax error: command needs an argument.");
			return CommandStat.FAILED;
		}
		if(connection.getUser().getUserStat() == UserStat.MISSING_USER) {
			connection.sendMessage("503 Login with USER first.");
			PrinterLog.printLog(Log.WARNING, LocalDateTime.now(), connection.getSocket().getInetAddress() + "/" + connection.getSocket().getPort() + " try to connect without username.");
			return CommandStat.FAILED;
		}
		if(connection.getUser().getUserStat() == UserStat.MISSING_PASS) {
			if(ServerMain.USER_MANAGER.validPassword(connection.getUser().getUsername(), args[1])) {
				connection.getUser().setUserStat(UserStat.CONNECTED_WITHOUT_DATAPORT);
				connection.sendMessage("230 Login successful.");
				PrinterLog.printLog(Log.INFO, LocalDateTime.now(), connection.getSocket().getInetAddress() + "/" + connection.getSocket().getPort() + " is connect as " + connection.getUser().getUsername());
			}
			else {
				connection.sendMessage("530 Authentication failed.");
				connection.getUser().setUserStat(UserStat.MISSING_USER);
				PrinterLog.printLog(Log.INFO, LocalDateTime.now(), connection.getSocket().getInetAddress() + "/" + connection.getSocket().getPort() + " failed to connect as " + connection.getUser().getUsername());
				return CommandStat.FAILED;
			}
		}
		return CommandStat.SUCCESSED;
	}
}