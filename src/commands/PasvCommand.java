package commands;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;
import util.UserStat;

public class PasvCommand implements CommandExecutor {

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		
		if (args.length <= 1) {
			connection.sendMessage("501 Syntax error: command needs an argument.");
			return CommandStat.FAILED;
		}
		if(!(connection.getUser().getUserStat() == UserStat.CONNECTED || connection.getUser().getUserStat() == UserStat.CONNECTED_WITHOUT_DATAPORT)) {
			connection.sendMessage("530 Log in with USER and PASS first.");
			return CommandStat.FAILED;
		}
		
		connection.sendMessage("500 WIP");
		return CommandStat.WIP_COMMAND;
	}
}
