package commands;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import main.Connection;
import main.ServerMain;
import util.CommandExecutor;
import util.CommandStat;
import util.Log;
import util.UserStat;

public class PortCommand implements CommandExecutor{
	
	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		String[] address = args[1].split(".");
		if (args.length <= 1) {
			connection.sendMessage("501 Syntax error: command needs an argument.");
			return CommandStat.FAILED;
		}
		if(!(connection.getUser().getUserStat() == UserStat.CONNECTED || connection.getUser().getUserStat() == UserStat.CONNECTED_WITHOUT_DATAPORT)) {
			connection.sendMessage("530 Log in with USER and PASS first.");
			return CommandStat.FAILED;
		}
		if(args[0].split(",").length != 6) {
			connection.sendMessage("501 Invalid PORT format.");
			return CommandStat.FAILED;
		}
		
		String ip = getIp(address);
		int port = getPort(address);		
		
		
		connection.sendMessage("500 WIP");
		return CommandStat.WIP_COMMAND;
	}
	
	private int getPort(String[] args) {
		return Integer.parseInt(args[4]) * 256 + Integer.parseInt(args[5]);
	}

	private String getIp(String[] args) {
		String ip = args[0];
		for(int i = 1; i < 4; i++)
			ip += "." + args[i];
		return ip;
	}
}
