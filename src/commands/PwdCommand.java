package commands;

import java.io.File;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;
import util.UserStat;

public class PwdCommand implements CommandExecutor{

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		if(!(connection.getUser().getUserStat() == UserStat.CONNECTED || connection.getUser().getUserStat() == UserStat.CONNECTED_WITHOUT_DATAPORT)) {
			connection.sendMessage("530 Log in with USER and PASS first.");
			return CommandStat.FAILED;
		}
		File path = connection.getUser().getCurrentPath();
		connection.sendMessage("257 \""+ path.getAbsolutePath() +"\" is the current directory.");
		return CommandStat.SUCCESSED;
	}
}
