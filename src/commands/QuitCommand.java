package commands;


import java.io.IOException;
import java.time.LocalDateTime;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;
import util.Log;
import util.PrinterLog;

public class QuitCommand implements CommandExecutor {

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		try {
			connection.sendMessage("221 Goodbye.");
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), connection.getSocket().toString() + " disconnected");
			connection.getSocket().close();
		} catch (IOException e) {
			if(!connection.getSocket().isClosed())
				return CommandStat.FAILED;
		}
		return CommandStat.SUCCESSED;
	}

}
