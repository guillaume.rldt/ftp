package commands;

import java.time.LocalDateTime;

import main.Connection;
import util.CommandExecutor;
import util.CommandStat;
import util.Log;
import util.PrinterLog;
import util.UserStat;

public class UserCommand implements CommandExecutor{

	@Override
	public CommandStat executeCommand(Connection connection, String command, String[] args) {
		if (args.length <= 1) {
			connection.sendMessage("501 Syntax error: command needs an argument.");
			return CommandStat.FAILED;
		}

		if(connection.getUser().getUserStat() == UserStat.MISSING_USER || connection.getUser().getUserStat() == UserStat.MISSING_PASS) {
				connection.getUser().setUsername(args[1]);
				connection.getUser().setUserStat(UserStat.MISSING_PASS);
				connection.sendMessage("331 Username ok, send password");
				PrinterLog.printLog(Log.INFO, LocalDateTime.now(), connection.getSocket().getInetAddress() + "/" + connection.getSocket().getPort() + " try to connect as " + args[0]);
		}
		return CommandStat.SUCCESSED;
	}

}
