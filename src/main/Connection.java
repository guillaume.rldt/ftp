package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;

import util.Log;
import util.PrinterLog;
import util.User;

public class Connection {

	private User user;
	private Socket client;

	private PrintWriter out;
	private BufferedReader in;

	public Connection(Socket client) throws IOException {
		user = new User();
		this.client = client;
		out = new PrintWriter(client.getOutputStream());
		in = new BufferedReader(new InputStreamReader(client.getInputStream()));
	}

	public void sendMessage(String message) {
		out.println(message);
		out.flush();
		PrinterLog.printLog(Log.INFO, LocalDateTime.now(), message + " sended" + " to " + user.getUsername() + " on " + client.getInetAddress());
	}

	public String readMessage() {
		String message = "";
		try {
			message = in.readLine();
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), message + " receved" + " by " + user.getUsername() + " on " + client.getInetAddress());
		} catch (IOException e) {
			PrinterLog.printLog(Log.ERROR, LocalDateTime.now(), "Message cannot be read");
			PrinterLog.printLog(Log.ERROR, LocalDateTime.now(), e.getMessage());
			sendMessage("Your message cannot be read");
		}
		return message;
	}

	public User getUser() {
		return user;
	}

	public Socket getSocket() {
		return client;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
}
