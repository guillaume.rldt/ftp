package main;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;

import commands.*;
import util.ConnectionTreatment;
import util.CommandStat;
import util.Log;
import util.PrinterLog;
import util.UserManager;

public class ServerMain {

	private static final int DEFAULT_PORT_SERVICE = 2121;
	private static final int DEFAULT_PORT_DATA = 2120;
	public static int portService;
	public static int portData;

	private static final File DEFAULT_FOLDER = new File("").getAbsoluteFile();
	public static InetAddress ADDRESS;

	public static ServerSocket serverSocket;
	public static final UserManager USER_MANAGER = new UserManager();

	public static void init(String[] args) {
		if (args.length >= 1) {
			portService = Integer.parseInt(args[0]);
		}
		else portService = DEFAULT_PORT_SERVICE;
		portData = DEFAULT_PORT_DATA;
		try {
			ADDRESS = InetAddress.getLocalHost();
			serverSocket = new ServerSocket(portService);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), "Starting FTP serveur on " + serverSocket.getLocalSocketAddress());
		} catch (UnknownHostException e) {
			PrinterLog.printLog(Log.ERROR, LocalDateTime.now(), "UnknownHostException");
			System.exit(1);
		} catch (IOException e) {
			PrinterLog.printLog(Log.ERROR, LocalDateTime.now(), "ServerSocket/Port");
			System.exit(1);
		}
	}

	public static void initUserManager() {
		USER_MANAGER.addUser("ANONYMOUS", "");
		USER_MANAGER.addUser("USER", "12345");
	}

	public static Socket listen() throws IOException {
		return serverSocket.accept();
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		init(args);
		PrinterLog.printLog(Log.INFO, LocalDateTime.now(), "Server connected");
		initUserManager();
		PrinterLog.printLog(Log.INFO, LocalDateTime.now(), "Principal folder : " + DEFAULT_FOLDER.getAbsolutePath());
		PrinterLog.printLog(Log.INFO, LocalDateTime.now(), "Waiting client to connect");

		for(;;) {
			Socket client = listen();
			Connection connection = new Connection(client);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), "Connection from client " + client.getInetAddress() + "/" + client.getPort());
			Thread t = new Thread(new ConnectionTreatment(connection));
			t.start();
		}
	}

	public static void executeCommand(Connection connection, String cmd, String[] args) {
		CommandStat cs;
		switch (cmd.toUpperCase()) {
		case "USER":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new UserCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "PASS":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new PassCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "HELP":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new HelpCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "CWD":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new CwdCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "PASV":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new PasvCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "PORT":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new PortCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "QUIT":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new QuitCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "LIST":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new ListCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "MODE":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new ModeCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "NOOP":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new NoopCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "RETR":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new RetrCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "STOR":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new StorCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "TYPE":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new TypeCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		case "PWD":
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername());
			cs = new PwdCommand().executeCommand(connection, cmd, args);
			PrinterLog.printLog(Log.INFO, LocalDateTime.now(), cs.getMessage() + " of " + cmd.toUpperCase() + " by " + connection.getUser().getUsername());
			break;
		default:
			connection.sendMessage("500 Command " + cmd + " not understood.");
			PrinterLog.printLog(Log.WARNING, LocalDateTime.now(), cmd.toUpperCase() + " command receved by " + connection.getUser().getUsername() + " was not understood");
			break;
		}
	}

	public static int getDefaultPortService() {
		return DEFAULT_PORT_SERVICE;
	}

	public static int getDefaultPortData() {
		return DEFAULT_PORT_DATA;
	}

	public static File getDefaultFolder() {
		return DEFAULT_FOLDER;
	}
	
}
