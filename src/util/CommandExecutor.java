package util;

import main.Connection;

public interface CommandExecutor {
	public CommandStat executeCommand(Connection connection, String command, String[] args);
}
