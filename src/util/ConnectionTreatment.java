package util;

import java.util.Arrays;

import main.Connection;
import main.ServerMain;

public class ConnectionTreatment implements Runnable{

	private Connection connection;
	
	public ConnectionTreatment(Connection connection) {
		this.connection = connection;
	}
	
	@Override
	public void run() {
		this.connection.sendMessage("220 Your connected on FTP");
		
		while(true) {
			if (connection.getSocket().isClosed()) break;
			String[] args = {};
			String cmd = "";
			//traitement des commandes
			String message = connection.readMessage().toUpperCase();
			String[] messageSplited = message.split(" ");
			if (messageSplited.length < 1) {
				connection.sendMessage("Missing command");
				continue;
			}
			cmd = messageSplited[0];
			args = messageSplited;
			
			ServerMain.executeCommand(connection, cmd, args);
			
		}
	}
}
