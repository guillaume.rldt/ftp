package util;

public enum Log {

	INFO ("[Info]"),
	WARNING ("[Warning]"),
	ERROR ("[Error]");
	
	private String message;
	
	Log(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
