package util;

import java.time.LocalDateTime;

public class PrinterLog {
	
	public static void printLog(Log type, LocalDateTime date, String message) {
		System.out.println("[" + date + "]" + type.getMessage() + " " +message);
	}
}
