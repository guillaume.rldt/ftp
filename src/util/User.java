package util;

import java.io.File;

import main.ServerMain;

public class User {

	private String username;
	private String password;
	
	private File currentPath;
	private UserStat userStat;
	
	public User() {
		this.username = null;
		this.password = null;
		this.currentPath = ServerMain.getDefaultFolder();
		userStat = UserStat.MISSING_USER;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	
	public File getCurrentPath() {
		return currentPath;
	}
	
	public void setCurrentPath(File currentPath) {
		this.currentPath = currentPath;
	}
	
	public UserStat getUserStat() {
		return userStat;
	}
	
	public void setUserStat(UserStat userStat) {
		this.userStat = userStat;
	}

	@Override
	public String toString() {
		return "User [username=" + username + "]";
	}
	
}
