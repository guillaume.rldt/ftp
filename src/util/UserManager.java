package util;

import java.util.ArrayList;
import java.util.List;

public class UserManager {

	private List<User> users;
	
	public UserManager() {
		this.users = new ArrayList<>();
	}
	
	public void addUser(User user) {
		this.users.add(user);
	}
	
	public void addUser(String username, String password) {
		User u = new User();
		u.setUsername(username);
		u.setPassword(password);
		this.users.add(u);
	}
	
	public User getUser(String username) {
		for(User user : users) {
			if(user.getUsername().equals(username))
				return user;
		}
		return null;
	}
	
	public boolean containsUsername(String username) {
		return getUser(username) != null;
	}
	
	public boolean validPassword(String username, String password) {
		if(!containsUsername(username))
			return false;
		User user = getUser(username);
		if(user.getPassword().isEmpty() || user.getPassword().equals(password))
			return true;
		
		return false;
	}
}
